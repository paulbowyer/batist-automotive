$(document).ready(function() {

	$('#searchform select').not("#searchform select:first").not("#searchform select:last").attr('disabled', true);

	if (finder) {
		
		$.each(finder, function(index, val) {
			 
			$('#selector-make').append('<option value="' + index + '">' + index +"</option>");

		});

		$('#selector-model').attr('disabled', true);

		$('#selector-make').on('change', function(){

			currentMake = $(this).val();


			$('#selector-model').empty().append('<option value="">Model</option>');
			$('#selector-model').attr('disabled', false);

			$('#selector-year').empty().append('<option value="">Year</option>');
			$('#selector-year').attr('disabled', true);

			$('#selector-engine').empty().append('<option value="">Engine</option>');
			$('#selector-engine').attr('disabled', true);

			
			$.each( finder[currentMake], function(index, val) {

				$('#selector-model').append('<option value="' + index + '">' + index +"</option>");

			});
			

			$('#selector-model').on('change', function(){
				
				currentModel = $(this).val();

				$('#selector-year').empty().append('<option value="">Year</option>');
				$('#selector-year').attr('disabled', false);

				$('#selector-engine').empty().append('<option value="">Engine</option>');
				$('#selector-engine').attr('disabled', true);


				if (finder[currentMake][currentModel]) {
					
					$.each( finder[currentMake][currentModel], function(index, val) {

						$('#selector-year').append('<option value="' + index + '">' + index +"</option>");

					});

				};


				$('#selector-year').on('change', function(){

					currentYear = $(this).val();
					
					$('#selector-engine').empty().append('<option value="">Engine</option>');
					$('#selector-engine').attr('disabled', false);

					if (finder[currentMake][currentModel][currentYear]['Engine']) {
						
						$('#selector-engine').append('<option value="">Exclude Engine</option>');

						$.each( finder[currentMake][currentModel][currentYear]['Engine'], function(index, val) {

							$('#selector-engine').append('<option value="' + val + '">' + val +"</option>");

						});

					} ;


					$('#selector-engine').on('change', function(){

						var currentEngine = $(this).val();
						
						console.log( currentMake + "\t" +  currentModel + "\t" + currentYear + "\t" + currentEngine);

					});

				});

			});

		});

	};


});